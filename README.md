`poor man's dedup` is a tool to find duplicate files under a given
folder.

This tool was written just for the fun to write Bash scripts using
common Shell tools to accomplish this task.

It is useful for bakups and/or managing a huge volume of personal files.

It obviously will be no match with more sophisticate and performant
tools but it will do the job, more slowly though.


Output of `ls` long format:

- permissions
- number of linked hard-links
- owner of the file
- to which group this file belongs to
- size
- modification/creation date and time
- file/directory name


Final output we want:

- hash
- size
- file/directory name
- permissions
- number of linked hard-links
- owner of the file
- to which group this file belongs to
- modification/creation date and time
